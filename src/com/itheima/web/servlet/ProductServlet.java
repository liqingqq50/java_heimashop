package com.itheima.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import com.google.gson.Gson;
import com.itheima.domain.Cart;
import com.itheima.domain.CartItem;
import com.itheima.domain.Category;
import com.itheima.domain.OrderItem;
import com.itheima.domain.Order;
import com.itheima.domain.PageBean;
import com.itheima.domain.Product;
import com.itheima.domain.User;
import com.itheima.service.ProductService;
import com.itheima.utils.CommonsUtils;
import com.itheima.utils.JedisPoolUtils;
import redis.clients.jedis.Jedis;

/**
 * Servlet implementation class ProductServlet
 */
public class ProductServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
//	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		doGet(request, response);
//	}

	/**
	 *	 获取分类列表数据
	 */
	public void categoryList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ProductService productService = new ProductService();

		Jedis jedis = JedisPoolUtils.getJedis();
		String categoryListJson = jedis.get("categoryListJson");

		if (categoryListJson == null) {
			System.out.println("缓存没有数据");
			List<Category> categoryList = productService.findAllCategoryList();

			Gson gson = new Gson();
			categoryListJson = gson.toJson(categoryList);
			jedis.set("categoryListJson", categoryListJson);
		}

		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(categoryListJson);
	}
	
	/**
	 *	 获取主界面的数据
	 */
	public void index(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ProductService productService = new ProductService();
		List<Product> hotProductList = productService.findHotProductList();
		List<Product> newProductList = productService.findNewProductList();

		request.setAttribute("hotProductList", hotProductList);
		request.setAttribute("newProductList", newProductList);

		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	
	/**
	 *	 获取产品列表数据
	 */
	public void productListByCid(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cid = request.getParameter("cid");
		String currentPage = request.getParameter("currentPage");
		ProductService productService = new ProductService();

		if (currentPage == null)
			currentPage = "1";
		int currentCount = 12;

		PageBean pageBean = productService.findProductListByCid(cid, Integer.valueOf(currentPage), currentCount);

		List<Product> historyProductList = new ArrayList<Product>();
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("pids")) {
					String pids = cookie.getValue();
					String[] split = pids.split("-");
					for (int i = 0; i < split.length&&i<7; i++) {
						Product product = productService.findProductByPid(split[i]);
						historyProductList.add(product);
					}
				}

			}
		}

		request.setAttribute("historyProductList", historyProductList);
		request.setAttribute("pageBean", pageBean);
		request.setAttribute("cid", cid);

		request.getRequestDispatcher("/product_list.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	/**
	 *	 获取单个产品详情数据
	 */
	public void productInfo(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pid = request.getParameter("pid");
		String cid = request.getParameter("cid");
		String currentPage = request.getParameter("currentPage");
		ProductService productService = new ProductService();
		Product product = productService.findProductByPid(pid);

		String pids = pid;

		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("pids")) {
					String pidCokie = cookie.getValue();
					String[] split = pidCokie.split("-");
					List<String> list = Arrays.asList(split);
					LinkedList<String> linkedList = new LinkedList<>(list);
					if (linkedList.contains(pid)) {
						linkedList.remove(pid);
					}
					linkedList.addFirst(pid);

					StringBuffer stringBuffer = new StringBuffer();
					for (int i = 0; i < linkedList.size()&&i<7; i++) {
						stringBuffer.append(linkedList.get(i));
						stringBuffer.append("-");
					}
					stringBuffer.substring(0, stringBuffer.length()-1);
					pids= stringBuffer.toString();
				}
				
			}
		}
		

		Cookie cookie = new Cookie("pids", pids);
		response.addCookie(cookie);

		request.setAttribute("product", product);
		request.setAttribute("cid", cid);
		request.setAttribute("currentPage", currentPage);
		request.getRequestDispatcher("/product_info.jsp").forward(request, response);
	}
	
	/**
	 *	 将商品添加进购物车
	 */
	public void addProductToCart(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String pid = request.getParameter("pid");
		int buyNum = Integer.parseInt(request.getParameter("buyNum"));
		
		
		ProductService productService = new ProductService();
		Product product = productService.findProductByPid(pid);
		
		Map<String, CartItem> cartItems = null;
		CartItem cartItem = null;
		
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		double subtotal =0.0;
		double total =buyNum*product.getShop_price();;
	
		if(cart!=null) {
			cartItems = cart.getCartItems();
			
			if(cartItems.containsKey(pid)) {
				cartItem = cartItems.get(pid);
				subtotal = cartItem.getSubtotal()+(buyNum*product.getShop_price());
				buyNum+=cartItem.getBuyNum();
				
			}else {
				cartItem = new CartItem();
				cartItem.setProduct(product);
				subtotal = product.getShop_price()*buyNum;
			}
			
		}else {
			cart = new Cart();
			cartItems = new HashMap<>();
			cartItem = new CartItem();
			cartItem.setProduct(product);
			subtotal = product.getShop_price()*buyNum;
			cartItem.setSubtotal(product.getShop_price()*buyNum);
		}
		
		cartItem.setBuyNum(buyNum);
		cartItem.setSubtotal(subtotal);
		cartItems.put(pid, cartItem);
		
		cart.setCartItems(cartItems);
		cart.setTotal(cart.getTotal()+total);
		
		session.setAttribute("cart", cart);
		
		response.sendRedirect(request.getContextPath()+"/cart.jsp");
		
	}
	
	/**
	 *  	删除购物车单项商品
	 */
	public void deleteCartItem(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pid = request.getParameter("pid");
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		if(cart!=null) {
			Map<String, CartItem> cartItems= cart.getCartItems();
			if(cartItems.containsKey(pid)) {
				CartItem cartItem = cartItems.get(pid);
				cart.setTotal(cart.getTotal()-cartItem.getSubtotal());
				cartItems.remove(pid);
				cart.setCartItems(cartItems);
				session.setAttribute("cart", cart);
			}
			
		}
		
		response.sendRedirect(request.getContextPath()+"/cart.jsp");
	}
	
	
	/**
	 *  	清空购物车
	 */
	public void clearCart(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		if(cart!=null) {
			session.removeAttribute("cart");
		}
		
		response.sendRedirect(request.getContextPath()+"/cart.jsp");
	}
	
	/**
	 *  	提交订单
	 */
	public void submitOrder(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		//暂时不加登录判断
		User user = (User) session.getAttribute("user");
		if(user==null) {
			response.sendRedirect(request.getContextPath()+"/login.jsp");
			return;
		}
		
		Cart cart = (Cart) session.getAttribute("cart");
		if(cart!=null) {
			Order order = new Order();
			
			order.setOid(CommonsUtils.getUUID());
			order.setOrdertime(new Date());
			order.setTotal(cart.getTotal());
			order.setState(0);
			order.setAddress(null);
			order.setName(null);
			order.setTelephone(null);
			order.setUser(user);
			
			Map<String, CartItem> cartItems = cart.getCartItems();
			
			List<OrderItem> orderItems = order.getOrderItems();
			for(Map.Entry<String, CartItem> entry:cartItems.entrySet()) {
				CartItem cartItem = entry.getValue();
				OrderItem orderItem = new OrderItem();
				orderItem.setItemId(CommonsUtils.getUUID());
				orderItem.setCount(cartItem.getBuyNum());
				orderItem.setOrder(order);
				orderItem.setProduct(cartItem.getProduct());
				orderItem.setSubtotal(cartItem.getSubtotal());
				
				orderItems.add(orderItem);
			}
			
			ProductService productService = new ProductService();
			productService.submitOder(order);
			session.setAttribute("order", order);
			
		}
		
		response.sendRedirect(request.getContextPath()+"/order_info.jsp");
		
	}
	
	/**
	 *  	确认订单
	 */
	public void confirmOrder(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Map<String, String[]> properties = request.getParameterMap();
		
		Order order = new Order();
		try {
			BeanUtils.populate(order, properties);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		ProductService productService = new ProductService();
		productService.updateOrderAdrr(order);
		
	}
}

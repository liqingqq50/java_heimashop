package com.itheima.utils;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class C3P0Test {
	public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            try {
                conn = DataSourceUtils.getConnection();
                ps = conn.prepareStatement("select * from user");
                rs = ps.executeQuery();

                while (rs.next()) {
                    System.out.println(
                            rs.getString(1) + " , " +
                                    rs.getString(2) + " , " +
                                    rs.getString(3)
                    );
                }
            } finally {
//                C3P0Utils.close(conn, ps, rs);
            	DataSourceUtils.closeConnection();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

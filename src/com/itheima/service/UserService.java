package com.itheima.service;

import java.sql.SQLException;

import com.itheima.dao.UserDao;
import com.itheima.domain.User;

public class UserService {

	public boolean regist(User user) {
		UserDao dao = new UserDao();
		int row = 0;
		try {
			row = dao.regist(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return row>0?true:false;
	}

	public void active(String activeCode) {
		UserDao userDao = new UserDao();
		try {
			userDao.active(activeCode);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public boolean checkUsername(String username) {
		UserDao userDao = new UserDao();
		long count = 0;
		try {
			count = userDao.checkUsername(username);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count>0;
	}

	public User login(String username, String password) {
		UserDao userDao = new UserDao();
		User user = null;
		try {
			user = userDao.login(username,password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

}
